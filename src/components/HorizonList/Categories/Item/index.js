/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import { withTheme, Tools, Config } from "@common";
import {TouchableScale} from '@components'
import styles from "./styles";
import { LinearGradient } from "@expo";

class Item extends PureComponent {

  render() {
    const {
      item,
      label,
      onPress,
      type,
      theme: {
        colors: { text },
      },
    } = this.props;

    return (
      <View style={styles.container}>
        <TouchableScale
          scaleTo={0.7}
          style={styles.wrap}
          onPress={() => onPress({ ...item, circle: true, name: label })}>
          <View style={[styles.background, {opacity: 1, backgroundColor: item.colors[0]} ]}/>

          <View style={styles.iconView}>
            <Image
                source={item.image}
                style={[styles.icon2, { tintColor: item.colors[1] }]}/>
          </View>
          <Text style={[styles.title, { color: text }]}>{label}</Text>
        </TouchableScale>
      </View>
    )
  }
}

export default withTheme(Item);
