/** @format */

import React, { PureComponent } from "react";
import { FlatList } from "react-native";
import { withTheme } from "@common";
import Item from "./Item";

class MainCategories extends PureComponent {
  static defaultProps = {
    categories: [],
    items: [],
  };

  state={selectedIndex:0}

  render() {
    const { categories, items, type, onPress, config } = this.props;
    const mapping = {};
    
    categories.forEach((item) => {
      mapping[item.id] = item.name;
    });
    const column = typeof config.column !== 'undefined' ? config.column : 1

    return categories.length > 0 && <FlatList
    keyExtractor={(item, index) => `${index}`}
    contentContainerStyle={styles.flatlist}
    showsHorizontalScrollIndicator={false}
    extraData={this.state}
    horizontal={column === 1}
    numColumns={column}

    data={items}
    renderItem={({ item,index }) => {
      return (
        <Item item={item} index={index} item={item} selectedIndex={this.state.selectedIndex} ClickME={()=>{
          console.log('press', index)
          this.setState({
            selectedIndex: index
          })
        }} type={type} label={mapping[item.category]} onPress={onPress} />
      )
    }}
  />
  }
}

const styles = {
  flatlist: {
    marginBottom: 10,
  }
}

export default withTheme(MainCategories);
