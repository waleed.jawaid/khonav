/** @format */

import React from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";

export default CustomIndex = (props) => {
    const {
        item,
        label,
        onPress,
        type,
        index,
        selectedIndex,
    } = props;

    console.log('propssss', selectedIndex)

    return (
        <View style={styles.container}>
          <TouchableOpacity style={index == selectedIndex ? {
            backgroundColor: '#000',
            width: 90,
            height: 35,
            borderRadius:8,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor:"#000",
            borderWidth:0.5,
            marginBottom:5
          }: {
            backgroundColor: '#fff',
            width: 90,
            height: 35,
            borderRadius:8,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor:"#000",
            borderWidth:0.5,
            marginBottom:5
          }
        }
        onPress={props.ClickME}
        >
            <Text style={index == selectedIndex ? {color: '#fff'}: {color: '#000'}}>{label}</Text>
          </TouchableOpacity>
        </View>
    )
}